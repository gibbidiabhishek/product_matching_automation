import sys
sys.path.insert(0, 'src/')
import json
import requests
import time
import gzip
import hashlib
from datetime import datetime
try:
	from fluent import sender
	from fluent import event	
except Exception as e:
	print ("Unable to import loggeer")
	# raise e

import socket
import unicodedata
import sys
from mysqlOperations import SqlOperations

try:
    from kafkalib import kafka_consumer
except Exception as e:
    print ("Unable to import kafka_consumer",e)


def get_ip_address():
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        return s.getsockname()[0]

#def logData(data):
        ## will push data to elstic search
    #    print data
 #       sender.setup("cbir_indexing",host="localhost",port=24224)
 #       event.Event('status',data)





def writeJsonFile(listOfDataToPush,seed_file_path):
	with open(seed_file_path,"w") as fileWriter:
		for itr in listOfDataToPush:
			json.dump(itr,fileWriter)
			fileWriter.write("\n")




def main():

	# Initializing all 3 consumers for each tech
	print ("Initializing kafka consumer for CBIR")
	topic_cbir = "sm_output_cbir_clustering"
	consumer_cbir = "productMatchingInputCBIR"
	kafkaConsumerCBIR =  kafka_consumer.kafka(topic = topic_cbir,consumer_group=consumer_cbir,read_from_beginning=False,runForever=False)
	consumer_CBIR = kafkaConsumerCBIR.get_consumer()

	print ("Initializing kafka consumer for TEXT")
	topic_text = "sm_output_text_clustering"
	consumer_text = "productMatchingInputText"
	kafkaConsumerText =  kafka_consumer.kafka(topic = topic_text,consumer_group=consumer_text,read_from_beginning=False,runForever=False)
	consumer_TEXT = kafkaConsumerText.get_consumer()

	print ("Initializing kafka consumer for PPC")
	topic_ppc = "sm_output_ppc_clustering"
	consumer_ppc = "productMatchingInputPPC"
	kafkaConsumerPPC =  kafka_consumer.kafka(topic = topic_ppc,consumer_group=consumer_ppc,read_from_beginning=False,runForever=False)
	consumer_PPC = kafkaConsumerPPC.get_consumer()

#	ipAdd = get_ip_address()


	error  = 0
	count = 0

	sqloperations = SqlOperations()

#	consumer_list = [kafkaConsumerCBIR,kafkaConsumerText,kafkaConsumerPPC]
	
	consumer_list = [consumer_CBIR,consumer_TEXT,consumer_PPC]

	tech_list = ["CBIR","TEXT","PPC"]
	# tech_list = ["TEXT"]

	table_name = 'URLH_TRACKING_TABLE'
	print ("Starting consuming data for CBIR/TEXT/PPC")
	
	while(True):
		for consumer,tech in zip(consumer_list,tech_list):
			listOfDataToPush = []
			print ("Trying to consume data from ",tech,len(listOfDataToPush))
			## for msg in consumer do the processing 
			for msg in consumer:

				value = msg.value.decode("utf-8")

				# print (value)
				# exit(-1)
				count+=1
				try:
					data = json.loads(value)

					to_sql_post = {}
					to_sql_post['primary_keys'] = {}
					to_sql_post['primary_keys']['account_ID'] = data['account_ID']
					to_sql_post['primary_keys']['urlh'] = data['seed']
					to_sql_post['primary_keys']['tech_to_run'] = tech

					to_sql_post['keys_to_update'] = {}
					to_sql_post['keys_to_update']['clustering_status'] = "TRUE"				
					listOfDataToPush.append(to_sql_post)


					# Run either when batch is of 1000 size or when for sometime no new requests are coming(handling last batch < bachsize)
					if len(listOfDataToPush) >=100 :
						print (listOfDataToPush[0])
						print ("pushing batch from: ",count,"error count",error)
						sqloperations.updateEntry(table_name,listOfDataToPush)
						listOfDataToPush=[]
						print ("pushing done for: ",count,"error count",error)
					else:
						continue

				except Exception as e:
					print ("error",e)
					# print ('data',data)
					error+=1
					continue 

			else:
				try:
					#only executing if we have some leftovers
					if len(listOfDataToPush) >1 :
						print (listOfDataToPush[0])
						print ("pushing batch from: ",count,"error count",error)

						sqloperations.updateEntry(table_name,listOfDataToPush)
						listOfDataToPush=[]
						print ("pushing done for: ",count,"error count",error)

				except Exception as e:
					print ("error",e)
					# print ("data",data)
					error+=1




if __name__ == "__main__":
	main()
