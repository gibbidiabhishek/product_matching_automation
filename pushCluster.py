# -*- coding: utf-8 -*-
# @Author: abhishek
# @Date:   2019-02-01 12:59:05
# @Last Modified by:   abhishek
# @Last Modified time: 2019-02-01 15:40:18

import json
from kafkalib import kafka_producer
import sys 
import argparse
import requests
import urllib
import sys

def excepthook(type, value, traceback):
    print(value)



############# exception handling  for release mode comment it for debug ###############################
sys.excepthook = excepthook
#####################################################################################################


def sendingMail(body,subject):
	mail_parameter = {
					#"to": "shubham@dataweave.com,byom@dataweave.com,anshul.garg@dataweave.com,kanchan.sarkar@dataweave.com,abhishek.gibbidi@dataweave.com,sanket@dataweave.com",
					"to": "abhishek.gibbidi@dataweave.com,anshul.garg@dataweave.com",
					"from": "reports@dataweave.com",
					"subject": subject,
					"body": body
	}

	sendReq = requests.post('http://api.priceweave.com/mail/mailer2.php?mail_parameter='+urllib.quote_plus(json.dumps(mail_parameter)))

def main(fileNameToRead):

#	"Code to push data to kafka"
	kafkaObj =kafka_producer.kafka() 



	count = 0
	with open(fileNameToRead,"r") as fileNameReader,open("testCLuster2.json","w") as outfile:
		for line in fileNameReader:
			data = json.loads(line)
			count+=1
			print (count)
			data["userid"] = "1126"
			json.dump(data,outfile)
			outfile.write("\n")
			# data["docs"] = [data["urlh"]]
			# kafkaObj.send("pr_bundle_upload_priceweave",[data],{})
	kafkaObj.close()

	# try:
	# 	body = "Simdel Indexing started for " + str(simdelId)
	# 	body += "\n Check the Simdel status in GPU"
	# 	subject = "Simdel Indexing Started"
	# 	sendingMail(body, subject)		
	# except Exception as e:
	# 	print "Exception: ",str(e)


if __name__ =="__main__":

	fileNameToRead = sys.argv[1]
	main(fileNameToRead)
