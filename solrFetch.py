# -*- coding: utf-8 -*-
# @Author: abhishek
# @Date:   2019-02-04 15:48:14
# @Last Modified by:   abhishek
# @Last Modified time: 2019-04-24 13:05:53

import json
import requests

def fetchSolrEntr(urlhList,solrAdd,country):

	fetchLimit = len(urlhList)
	outputList = []
	for idx in range(0,len(urlhList),fetchLimit):
		candHashList =  urlhList[idx:idx+fetchLimit]
		print ("candHashList: ",candHashList,urlhList)
		queryString = "( "+ " ".join(candHashList)+ " )"
		start = 0
		rows = fetchLimit
		# print queryString
		while(1):
			try:
				param = {}
				param["q"] = "urlh:%s"%(queryString)
				param["wt"] = "json"
				param["start"] = start
				param["rows"] = rows
				param["country"] = country
				dataFetched = requests.post(solrAdd,data=param)
				print (dataFetched.url, param)
				dataFetchedJson =dataFetched.json()
				print (dataFetchedJson)
				if "docs" in dataFetchedJson:
					if len(dataFetchedJson["docs"])==0:
						break
					dataFetchedJson = dataFetchedJson["docs"]
					for entry in dataFetchedJson:
						# comp_urlh = entry["urlh"]
						# if comp_urlh not in outputList:
							# outputList[comp_urlh] = entry 	
						outputList.append(entry)
				start = start+rows				
			except Exception as e:
				print (e)


	# print "Done"		
	return outputList


if __name__ == '__main__':
	print ("Working")
	
	# print fetchSolrEntr(urlhList=["3c7f8bb66e88e9de4fe704d2ae30bbc9afea7f85","62ecb7d59e672a25fd35c7c27a39a337b9c18d1b"], 
		          # solrAdd= "http://api.solr.dweave.net/select",country = "usa")
