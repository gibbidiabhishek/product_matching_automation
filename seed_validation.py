#-*- coding: utf-8 -*-

import sys
import json
import csv
import os
import simplejson
import hashlib
import requests
import json
from collections import defaultdict
import random
import time
import ast
import copy 
# import numpy as np
start = time.time()
class validateSeed:
	def __init__(self,solrAdd,fetchLimit=1000,fileNameExt=""):
		
			self.solr = solrAdd
			self.fetchLimit = fetchLimit
			self.fileNameExt=fileNameExt
			self.results=dict()
			self.results['account_id'] = int()
			self.results['urlh'] = str()
			self.results['cluster'] = []
			self.avail_thumbnails = set()

			self.impression=[]



	def generate_thumbnail_hash(self, data_images,is_thumbnail=False):
#    urlList = []
		# print(data_images)
		thumbnail_list = []
		try:
			imageList = json.loads(data_images)
		except Exception as e:
			try:
				imageList = data_images
			except Exception as e:
				print (e)
				print (data)
			   
		if imageList is None:
			return [],[]
		else:
			pass

		if type(imageList) ==list:
			pass
		elif is_thumbnail:
			imageList = [imageList] # for handling comma in between images names
		else:
			imageList = imageList.split(",")

		urlDict = {}
		# print("imageList",imageList)
		for ent in imageList:
			
			ent = ent.encode('UTF-8')
			hashImageObj = hashlib.sha1(ent)
			hexImageHash = hashImageObj.hexdigest()
			urlDict[hexImageHash] = ent
			thumbnail_list.append(hexImageHash)
		return urlDict,thumbnail_list


	def fetchThumbnail(self,thumbnailList,solrAdd,session,flag):
		## thumbnail list to be checked 

		# print "In solr Query-------------------------------------------"
		outputList = []
		for idx in range(0,len(thumbnailList),self.fetchLimit):
			candHashList =  thumbnailList[idx:idx+self.fetchLimit]
			# print("thumbnail_list",thumbnailList)
			queryString = "( "+ " ".join(candHashList)+ " )"
			start = 0
			rows = self.fetchLimit
			while(1):
				param = {}
				param["q"] = "thumbnail_hash:%s"%(queryString)
				param["fl"] = "thumbnail_hash"
				param["wt"] = "json"
				param["start"] = start
				param["rows"] = rows
				dataFetched = session.post(solrAdd,data=param)
				# print dataFetched.url
				dataFetchedJson =dataFetched.json()
				# print(dataFetchedJson)
				if len(dataFetchedJson["response"]["docs"])==0:
					break
				outputList+=[data["thumbnail_hash"] for data in dataFetchedJson["response"]["docs"]]
				start = start+rows

		# print "Done"		
		return outputList

	#return a list of available thumbnails
	def getAvailableThumbnails(self,docs):
		image_needed_clusters = {'CBIR', 'SIMDEL'} 
		# print docs
		cluster_techs = set(docs[0]['clustering_tech']) 
		# print cluster_techs
		if len(image_needed_clusters.intersection(cluster_techs)) > 0: 
			Image_list = []
			for itr in docs:
				for k in  ['thumbnail', 'images']:
					data_images = itr['images']
					try:
						Image_list.append(itr['thumbnail'])
						imageList = json.loads(data_images)
					except Exception as e:
						try:
							imageList = data_images
						except Exception as e:
							print (e)
							print (data)
						   
					if imageList is None:
						return [],[]
					else:
						pass

					if type(imageList) ==list:
						pass
					elif is_thumbnail:
						imageList = [imageList] # for handling comma in between images names
					else:
						imageList = imageList.split(",")
					if len(imageList)>0:
						Image_list.append(imageList)
						# print itr['images']
			# print Image_list			
			session =  requests.Session()
			urlDict,thumbnail_list = self.generate_thumbnail_hash(Image_list)
			flag = False
			outputList = self.fetchThumbnail(thumbnail_list,solrAdd=self.solr,session=session,flag=flag)
			return outputList
		else :
			return []

	def image_key_validation(self,key, doc, validationType):
		images,thumbnail_list =self.generate_thumbnail_hash(doc[key])
					
		im_not_download = list(set(thumbnail_list) -self.avail_thumbnails)
		if bool(im_not_download):
						# print("these images are not downloaded", list(im_not_download))
			doc['validation_output'][validationType]['missing_'+key] = list(im_not_download)
			doc['validation_output'][validationType][key+'_downloaded'] = False
			return False

		else:
			doc['validation_output'][validationType][key+'_downloaded'] = True
			return True



	def single_key_validate(self, key,doc,validationType):
		null_list = [None, '', 'none', 'None']
		if key in doc.keys():
			
			if key in ['images','thumbnail']:
				return self.image_key_validation(key,doc,validationType)
			else:
				if doc[key] in null_list:
					return False
				else:
					return True
				
			
		else:
			return False
	


	def keys_Validation(self, needed_keys, optional_keys,validationType,doc):
		
		if validationType in ['CBIR', 'SIMDEL']:

			for im_key in ['images', 'thumbnail']:
				if im_key not in doc.keys():
					doc['validation_output'][validationType][im_key+' downloaded'] = 'NA'
				else: 
					doc['validation_output'][validationType][im_key+' downloaded'] = False

		
		# else:
		item_keys = []
		for key in needed_keys:
			avail = self.single_key_validate(key, doc,validationType)
			if avail:
				item_keys.append(key)
		missing_needed = set(needed_keys) - set(item_keys)
		doc['validation_output'][validationType]['missing_needed'] = list(missing_needed)
		# self.impression['missing_needed '+validationType] = list(missing_needed)
		if len(missing_needed)>0:
			doc['validation_output'][validationType]['status'] = 'FAIL'
		else:
			doc['validation_output'][validationType]['status'] = 'PASS'
		# optional_keys = ['images', 'brand', 'category']
		item_optional_keys = []
		for key in optional_keys:
			avail = self.single_key_validate(key, doc,validationType)
			if avail:
				item_optional_keys.append(key)
		missing_optional = set(optional_keys) - set(item_optional_keys)
		if doc['validation_output'][validationType]['status']=='FAIL':
			doc['validation_output'][validationType]['Error_Msg'] = "these keys "+str(list(missing_needed))+" are not available"
		doc['validation_output'][validationType]['missing_optional'] = list(missing_optional)
		print (doc['validation_output'])
		# self.impression.append(tech_dict)
		# self.impression['missing_optional '+validationType] = list(missing_optional)
		
	

			
	def cbir_validate(self,doc):
		needed_keys = ['title','url','urlh','seed_source','country','product_count_per_source','comp_list','account_ID','thumbnail']
		optional_keys = ['images', 'brand', 'category']
		validationType ='CBIR'
		doc['validation_output'][validationType] ={}
		if validationType not in doc['clustering_tech']:
			return 
		else:
			self.keys_Validation(needed_keys, optional_keys,validationType, doc)
		

	def simdel_validate(self,doc):
		needed_keys = ['title','url','urlh','seed_source','country','product_count_per_source','comp_list','account_ID','available_price', 'mrp', 'thumbnail']
		optional_keys = ['images', 'brand', 'category']
		validationType = 'SIMDEL'
		doc['validation_output'][validationType] ={}
		if validationType not in doc['clustering_tech']:
			return
		else:
			self.keys_Validation(needed_keys, optional_keys,validationType, doc)
		
	def text_validate(self,doc):
		needed_keys = ['title','url','urlh','seed_source','country','product_count_per_source','comp_list','account_ID', 'available_price','mrp']
		optional_keys = ['brand', 'meta', 'category']
		validationType = 'TEXT'
		doc['validation_output'][validationType] ={}
		if validationType not in doc['clustering_tech']:
			return
		else:
			# print "Need keys: ", needed_keys
			self.keys_Validation(needed_keys, optional_keys,validationType, doc)

	def ppc_validate(self, doc):
		needed_keys = ['title','url','urlh','seed_source','country','product_count_per_source','comp_list','account_ID', 'description','available_price','mrp']
		optional_keys = ['specifications','brand' , 'meta', 'category']
		validationType = 'PPC'
		doc['validation_output'][validationType] ={}
		if validationType not in doc['clustering_tech']:
			return
		else:
			self.keys_Validation(needed_keys, optional_keys,validationType, doc)

	def Validation(self,seed_list):
		print ("IN VALIDATION")
		# print(seed_list)
		try:
			self.avail_thumbnails = set(self.getAvailableThumbnails(seed_list))
			doc_list = []
			for doc in seed_list:
				# print(doc)
				# break
				# doc['account_id'] = random.sample(range(1,100000),1)[0]
				print ("CHECKING FOR VALIDATION")
				doc['validation_output'] = {}
				# self.simdel_validate(doc)
				self.text_validate(doc)
				# self.cbir_validate(doc)
				# self.ppc_validate(doc)
				# self.results['cluster'] = self.impression
				doc_list.append(doc)
			# print(doc_list)
			return doc_list			
		except Exception as e:
			raise e




			




def main():
	with open('file.txt', 'w') as file:
		seed_list = []
		with open('/home/semantics/data/product_matching_automation/data/test_data.json') as json_data:
			for itr in json_data:
				d = json.loads(itr)
				seed_list.append(d)
			v = validateSeed(solrAdd="http://172.17.1.64:8983/solr/warcMapper/select",fileNameExt="warc")
			output = v.Validation(seed_list)
			print(output[:10])
			file.write(json.dumps(output)+"\n")
			file.write("\n\n")
		

if __name__ == "__main__":
	main()
	end = time.time()
	print(end-start)
	 ## with if