import sys
import simplejson

from datetime import datetime,date,timedelta
import asyncio
import uvloop
from signal import signal, SIGINT
from sanic import Sanic
from sanic.response import json
import simplejson 
import os

app = Sanic()

try:
    sys.path.append('/home/DEV/')
    from kafkalib import kafka_consumer

except Exception as  e:
    print>>sys.stderr,('Failed to import Kafkalib : ' + str(repr(e)))
    sys.exit(1)


@app.route('/kafka/topics', methods=['POST','GET'])
async def topics_handler(request):
        req = request.raw_args
        return_reserved = False
        if('reserved_topics' in req and req['reserved_topics'] != None and req['reserved_topics'].lower() == 'true'):
            return_reserved = True
        reserved_topics = ["pl_da_all_crawls","pl_da_solr","pl_da_solr_retry","pl_da_solr_us","pl_da_solr_us_retry","kafka-monitor-topic","pr_sync_priceweave","pr_offers_priceweave","pl_da_testing"]
          
        rsp = {}
        sourceCountryMap = {}
        try :
            consumer = kafka_consumer.kafka(topic='pl_da_all_crawls',consumer_group = 'server_consumer').get_consumer()
        
            valid_topic = list(consumer.topics())
            consumer.close()
            docs = []
            for each_topic in valid_topic:
                consumer = kafka_consumer.kafka(topic=each_topic,consumer_group = 'server_consumer').get_consumer()
                d = {}
                d['is_reserved'] = False
                if(each_topic in reserved_topics):
                    d['is_reserved'] = True
                    if(return_reserved == False):
                        continue
                d['topic'] = each_topic
                d['partitions'] = len(consumer.partitions_for_topic(each_topic))
                docs.append(d)
            rsp['docs'] = docs
            rsp['status'] = 200

        except Exception as e:
            print(repr(e))
            rsp['error'] = repr(e)
            rsp['status']=400
        return json(rsp)





@app.route('/kafka/consumer_lag', methods=['POST','GET'])
async def consumer_lag(request):
        req = request.raw_args
        rsp = {}
        rsp['status'] = 200
        rsp['docs'] = {}
        time_start = datetime.now()
        topic = req.get('topic',None)
        cg = req.get('consumer_group',None)
        if(topic == None or cg == None):
            return json(rsp)

        api_rsp = get_lag_details(topic,cg)
        api_rsp['topic'] = topic
        api_rsp['consumer_group'] = cg
        rsp['docs'] = api_rsp

        rsp['qTime'] = (datetime.now()-time_start).total_seconds()

        return json(rsp)

def get_lag_details(topic,cg):
    k_consumer = kafka_consumer.kafka(topic = topic,consumer_group=cg)
    return k_consumer.get_lag()


if __name__ == "__main__":
    port = 8100
    print("Started Sanic API ")
    server = app.create_server(debug=False,host="0.0.0.0", port=port)
    asyncio.set_event_loop(uvloop.new_event_loop())
    loop = asyncio.get_event_loop()
    task = asyncio.ensure_future(server)
    signal(SIGINT, lambda s, f: loop.stop())
    try:
        loop.run_forever()
    except:
        loop.stop()

