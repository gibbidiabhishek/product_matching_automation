Consumers: Should be run only in GCE in kafka-dw project or a project where vpc peering exists. The kafka consumer is written in python3.x

Producer: A library used by crawlers / others to post data to Kafka. Producer can run in both python2.7 and python3.x
