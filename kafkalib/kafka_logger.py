try:
    from fluent import sender
    from fluent import event
except Exception,e:
    print  "Issue while importing central logger module",e


def kafka_logger(stat_data):
    try:
        sender.setup("kafka_response_log", host='localhost', port=24224)
        event.Event('status', stat_data)
    except Exception,e:
        print  "Issue while logging kafka post response log to elastic search",e


