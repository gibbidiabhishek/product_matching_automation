import sys
import simplejson as json

from datetime import datetime
from kafka import KafkaConsumer
from kafka import TopicPartition
import gzip
import logging
import socket

try:
    from fluent import sender
    from fluent import event
except Exception as e:
    print ("Issue while importing central logger module")
 
logging.basicConfig(level=logging.WARNING)

def print_stderr(log):
    print>>sys.stderr,log



class kafka:
    def __init__(self,topic,consumer_group,read_from_beginning=False,runForever=False,log_td_agent=False,client_id = "1"):
        self.hosts = ["kafka1.dwservices.com:9092","kafka2.dwservices.com:9092","kafka3.dwservices.com:9092"]
        consumer_config = {}
        consumer_config['bootstrap_servers']=self.hosts
        consumer_config['client_id']=client_id
        consumer_config['group_id']=consumer_group
        consumer_config['heartbeat_interval_ms']=5000
        #If there is any issue with committed offset, OffsetOutOfRange is given by Kafka. In this case, if auto_offset_reset is set to earliest, KafkaLibrary will start reading from oldest data. Setting it to default value. 
        #consumer_config['auto_offset_reset']="earliest"
        consumer_config['fetch_max_bytes']= 512*1024
        if(runForever == False):
            consumer_config['consumer_timeout_ms']=30000
        self.topic = topic
        self.consumer_group = consumer_group
        self.consumer = None
        self.log_td_agent=log_td_agent
        if(read_from_beginning == True):
            tp = []
            self.consumer = KafkaConsumer(**consumer_config)
            for i in range(10):
                tp.append(TopicPartition(topic, i))
            self.consumer.assign(tp)
            self.consumer.seek_to_beginning() 
        else:
            self.consumer = KafkaConsumer(topic,**consumer_config)
        self.logging={}
        try :
            self.logging['kf_hostname'] = socket.gethostname()
        except Exception as  e:
            self.logging['kf_hostname'] = 'unknown'
            pass
        try :
            self.logging['kf_machine_ip'] = socket.gethostbyname(self.logging['kf_hostname'])
        except Exception as e:
            self.logging['kf_machine_ip'] = "1.1.1.1"
            pass

    def __del__(self):
        self.consumer.commit()
        self.consumer.close()

    def get_consumer(self):
        return self.consumer
    
    def log_data(self,val_json={}):
        try:
            if(self.log_td_agent == False):
                return
            rsp = {}
            start_time = datetime.now()
            object_count = 1
            source = 'NA'
            tracking_code = 'manual'
            if(isinstance(val_json,list)):
                object_count = len(val_json)
                source = val_json[0].get('source','NA')
                tracking_code = val_json[0].get('tracking_code','manual')
            else:
                source = val_json.get('source','NA')
                tracking_code = val_json.get('tracking_code','manual')

            rsp['kf_timestamp'] = start_time.strftime('%Y%m%d%H%M%S')
            rsp['kf_content_length'] = len(json.dumps(val_json))
            rsp['kf_topic'] = self.topic
            rsp['kf_consumer_group'] = self.consumer_group
            rsp['kf_object_count'] = object_count
            rsp['kf_machine_ip'] = self.logging['kf_machine_ip']
            rsp['kf_hostname'] = self.logging['kf_hostname']
            rsp['kf_user'] = 'consumer'
            rsp['kf_source'] = str(source)
            rsp['kf_tracking_code'] = str(tracking_code)
            self.kafka_logger(rsp)

        except Exception as e:
            print ("ONE Issue while logging kafka post response log to elastic search :" + repr(e))
            pass
    def kafka_logger(self,stat_data):
        try:
            sender.setup("kafka_response_log", host='localhost', port=24224)
            event.Event('status', stat_data)
        except Exception as e:
            print ("Issue while logging kafka post response log to elastic search " + repr(e))
            pass
    def close(self):
        self.consumer.commit()
        return self.consumer.close()


    def get_lag(self):
        partitions = list(self.consumer.partitions_for_topic(self.topic))
        tp_list = []
        lag = {}
        
        for each_partition in partitions:
            tp = TopicPartition(self.topic, each_partition) 
            lag[each_partition] = {}
            tp_list.append(tp)
            beg_off = self.consumer.committed(tp)
            lag[each_partition]['beginning_offset'] = beg_off
        end_off = self.consumer.end_offsets(tp_list)
        total_lag = 0
        for key,val in end_off.items():
            #lag[key.partition]['end_offset'] = val
            p_lag = val - lag[key.partition]['beginning_offset']
            del lag[key.partition]['beginning_offset']
             
            lag[key.partition]['lag'] = p_lag
            total_lag += p_lag
        docs = {}
        docs['partitions'] = lag
        docs['total_lag'] = total_lag 
        return docs
         

#Pass topic as an argument
#Consumer group as second argument. Consumer group defines the behaviour of topic read.
#For Ex: python3.5 kafka_consumer.py amazon_assortments assortment_analysis
if __name__ == "__main__":
   
    topic = sys.argv[1]
    consumer_group = sys.argv[2]

    consumer = kafka(topic = topic,consumer_group=consumer_group)
#    lag = consumer.get_lag()
#    print (lag)
#    """
    counter = 0
    consumer = consumer.get_consumer()
    print("Reading on " + topic)
    file_name = "/tmp/kafka_test_" + topic+".gz"
    fw = gzip.open(file_name,"w")
    for msg in consumer:
        val = msg.value
        if(val == None):
            break
        counter += 1
        print (counter)
        print>>fw,json.dumps(json.loads(val))
#   """

