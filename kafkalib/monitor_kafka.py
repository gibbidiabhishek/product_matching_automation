from kazoo.client import KazooClient

import boto3
zk = KazooClient(hosts='zk1.dwservices.com:2181', read_only=True)
zk.start()
broker_ids = zk.get_children("/brokers/ids")
send_message = False
kafka_node_ids = ["1","2","3"]
kafka_msg_list = []
for each_kafka in kafka_node_ids:
    if(each_kafka not in broker_ids):
        send_message = True
        msg = "Kafka broker " + each_kafka + " is down"
        kafka_msg_list.append(msg)
if(send_message):
   print("Sending a message")    
   client = boto3.client('sns','us-east-1')
   client.publish(TopicArn='arn:aws:sns:us-east-1:765695734135:kafka',Subject='Kafka Node is down',Message="\n".join(kafka_msg_list))
zk.stop()
