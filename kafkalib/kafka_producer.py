import requests
import simplejson as json
from datetime import datetime
from kafka import KafkaProducer
from kafka import errors as Errors
import socket
import gzip
import sys  
import time

try:
    from fluent import sender
    from fluent import event
except Exception as e:
    print ("Issue while importing central logger module")
     
def kafka_logger(stat_data):
    try:
        sender.setup("kafka_response_log", host='localhost', port=24224)
        event.Event('status', stat_data)
    except Exception as e:
        #print ("Issue while logging kafka post response log to elastic search")
        pass
  
         
class kafka:
    def initiate_connection(self):
        self.kafka_producer= None
        self.hosts = ["kafka1.dwservices.com:9092","kafka2.dwservices.com:9092","kafka3.dwservices.com:9092"]
        producer_config_all = {}
        producer_config_all['bootstrap_servers']=self.hosts
        producer_config_all['compression_type']='gzip'
        producer_config_all['retries']=3
        self.kafka_producer = KafkaProducer(**producer_config_all)

    def __init__(self,log_td_agent=False):
        self.use_api = False
        self.topics_url = "http://api.kafka.dweave.net/topics"
        self.commit_size = 10
        try :
            self.initiate_connection()
        except Errors.NoBrokersAvailable as e:
            print(repr(e))
            self.use_api = True
        self.logging={}
        self.log_td_agent=log_td_agent
        #Max content length is 0.6MB
        self.max_content_length = 600000
        try :
            self.logging['kf_hostname'] = socket.gethostname()
        except Exception as  e:
            self.logging['kf_hostname'] = 'unknown'
            pass
        try :
            self.logging['kf_machine_ip'] = socket.gethostbyname(self.logging['kf_hostname'])
        except Exception as e:
            self.logging['kf_machine_ip'] = "1.1.1.1"
            pass


    def __del__(self):
        self.kafka_producer.close()

    def close(self):
        return self.kafka_producer.close()

    def get_available_topics(self):
        return requests.get(self.topics_url).json()
    
    def central_logger(self,topic,content_length,object_count,start_time,log_options,status_code,err=None,urlh=None):
        rsp = {}
        rsp['kf_api'] = self.use_api
        rsp['kf_timestamp'] = start_time.strftime('%Y%m%d%H%M%S')
        rsp['kf_time_taken'] = (datetime.now() - start_time).total_seconds()
        rsp['kf_content_length'] = content_length
        rsp['kf_topic'] = topic
        rsp['kf_object_count'] = object_count
        rsp['kf_machine_ip'] = self.logging['kf_machine_ip']
        rsp['kf_hostname'] = self.logging['kf_hostname']
        rsp['kf_user'] = 'producer'
        rsp['kf_source'] = str(log_options.get('source','NA'))
        rsp['kf_tracking_code'] = str(log_options.get('tracking_code','manual'))
        rsp['kf_job_id'] = str(log_options.get('job_id','00000'))
        rsp['kf_status_code'] = status_code
        if(urlh != None):
            rsp['kf_urlh'] = urlh
        if(status_code != 200 and err != None):
            rsp['kf_error'] = err
        kafka_logger(rsp)
        
    def send_in_parts(self,kafka_topic,doc,log_options):
        topic_list = list(set(kafka_topic.split(",")))
        doc_str = json.dumps(doc)
        content_length = len(doc_str)
        object_count = 1
        urlh = None
        if(isinstance(doc,list) ):
            try :
                urlh = doc[0].get('urlh',None)
            except Exception as e:
                print(repr(e))
            object_count = len(doc)
        else:
            try :
                urlh = doc.get('urlh',None)
            except Exception as e:
                print(repr(e))
            object_count = 1

        for topic in topic_list :
            start_time = datetime.now()
            status_code = 200
            err = None
            try:
                self.kafka_producer.send(topic,doc_str.encode())
            except Exception as e:
                err = repr(e)
                status_code = 400
            if(self.log_td_agent or content_length>self.max_content_length):
                self.central_logger(topic=topic,content_length=content_length,object_count=object_count,start_time = start_time,log_options=log_options,status_code=status_code,err=err,urlh=urlh)


    #kafka_topic is a "," separated string of topics. Only Available topics would be posted to Kafka
    def send(self,kafka_topic,doc,log_options):
        if(isinstance(doc,list) ):
            commit_size = self.commit_size
            if(doc[0].get('crawl_type','product') == 'product') :
                commit_size = 1
            start = 0
            while(start<len(doc)):
                self.send_in_parts(kafka_topic,doc[start:start+commit_size],log_options)
                start += commit_size
        else:
            self.send_in_parts(kafka_topic,doc,log_options)


if __name__ == "__main__":
    file_name = sys.argv[1]
    print("processing file " + file_name) 
    f = gzip.open(file_name)
    val = f.readline()
    k = kafka()
    counter = 0
    no_of_docs = 20
    val_json_l = []
    while val:
        counter += 1
        if(counter%1000 == 0):
            print("Processing " + str(counter))
        val_json = json.loads(val)
        
        log_options = {}
        val_json_l.append(val_json)
        topic = val_json['kafka_topic']
        if('pl_da_solr' in topic and len(val_json_l)>=no_of_docs):
            k.send('pl_da_solr',val_json_l,log_options)
            val_json_l = []
        if('pl_da_solr' not in topic):
            break
        val = f.readline()
    #k.close()
    time.sleep(50)
    """
    k = kafka()
    doc_list = [] 
    for i in range(10):
        doc = {}
        doc['url']=str(i)
        doc['urlh']='urlh_'+str(i)
        doc['crawl_date']='20180316'
        doc['source']='test'
        doc['crawl_type']='listing'
        doc['tracking_code']='dummy'
        doc_list.append(doc)
    log_options = {}
    log_options['source'] = doc['source']
    log_options['tracking_code'] = doc['tracking_code']
    k.send('pl_da_solr',doc_list,log_options)
    k.close()
    """
