# -*- coding: utf-8 -*-
# @Author: abhishek
# @Date:   2018-12-12 16:16:38
# @Last Modified by:   abhishek
# @Last Modified time: 2019-05-24 16:19:05


import sys
import json
import re
import requests
import cherrypy
import string
import time
import os
import argparse
import urllib
from collections import OrderedDict
from configobj import ConfigObj
from cherrypy.process.plugins import BackgroundTask


from seed_validation import validateSeed
from mysqlOperations import SqlOperations
from account_tracker import AccountTracker

class Distributor():

	def __init__(self,config_filepath):

		config = ConfigObj(config_filepath)

		self.printable = set(string.printable)

		self.warc_mapper = config["api"]["warc_mapper"]
		self.fetch_meta_api = config["api"]["tools_meta_api"]
		self.fetch_seed_api = config["api"]["tools_seed_api"]
		self.mailer_api = config["api"]["mailer_api"]

		self.to_mailid_list = config["mailids"]["to_mailids_list"]
		self.from_mailid = config["mailids"]["from_mailid"]

		self.validation_module = validateSeed(solrAdd=self.warc_mapper,fileNameExt="warc")
		self.clustering_tech_list = ["PPC","TEXT","CBIR"]
		self.sqloperations = SqlOperations()
		self.account_dict = {}
		self.account_list = []
		self.account_tracker = AccountTracker()
		task = BackgroundTask(interval = 0, function = self.fetchData, args = [],bus = cherrypy.engine)
		task.start()

	def sendingMail(self,body,subject):
		mail_parameter = {
					"to": self.to_mailid_list,
					"from": self.from_mailid,
					"subject": subject,
					"body": body
		}
		sendReq = requests.post(self.mailer_api+urllib.parse.quote_plus(json.dumps(mail_parameter)))


	@cherrypy.expose
	def showAccountStatus(self,**params):

		try:
			account_id = cherrypy.request.params['account_id']
			display_dict = self.account_tracker.showAccountStatus(account_id)
			return display_dict
		except Exception as e:
			display_dict = {}
			display_dict["Message"] = "Account is underprocessing. Please try after some time"
			return json.dumps(display_dict)

	@cherrypy.expose
	def startClustering(self,**params):

		account_id = cherrypy.request.params['account_id']
		try:
			tech_to_run = cherrypy.request.params['tech_to_run']
		except Exception as e:
			tech_to_run = "TEXT"

		tech_to_run_list = tech_to_run.split(",")

		print (tech_to_run_list)
		final_tech_list = []
		for tech in tech_to_run_list:
			print (tech.lower())
			tech = tech.strip()
			if tech.lower() in ["text"]:
				final_tech_list += ["TEXT"]
			elif tech.lower() in ["cbir","image"]:
				final_tech_list += ["CBIR"]
			else:
				temp_dict = {}
				temp_dict["status"] = 400
				temp_dict["status_message"] = "Fail"
				temp_dict["account_id"] = account_id
				temp_dict["message"] = "Unknow Value in the tech_to_run : " + str(tech_to_run)
				return json.dumps(temp_dict)
		tech_to_run = ",".join(final_tech_list)
		print (account_id,tech_to_run)
		temp_dict = {}
		temp_dict["status"] = 202
		temp_dict["status_message"] = "Success"
		temp_dict["account_id"] = account_id
		temp_dict["message"] = "Clustering has started for account id: " + str(account_id)

		self.account_list.append(account_id)
		self.account_dict[account_id] = tech_to_run
		print (self.account_dict)
		return json.dumps(temp_dict)

	def fetchData(self):


		for account_id in self.account_list:
			print ("IN FETCH DATA", account_id)
			tech_to_run = self.account_dict[account_id]

			print (tech_to_run)

			self.account_list.pop(self.account_list.index(account_id))
			self.account_dict.pop(account_id)

			print ("getting data")

			try:
				print (self.fetch_meta_api+str(account_id))
				response = requests.get(self.fetch_meta_api+str(account_id))
				response = response.json()
				print (response)
				response = response["data"]
				account_ID = response["userid"]
				account_name = response["username"]
				seed_source = response["seed_source"]

				self.sqloperations.insertToAccountTracking(account_ID,account_name,seed_source,tech_to_run)
			except Exception as e:
				print ("error in account insertToAccountTracking: ", e)
				raise e
			account_info_dict = self.sqloperations.getAccountInfo(account_ID=account_id)
			print (account_info_dict)
			try:

				params = {}
				params["userid"] = account_id
				params["start"] = 0
				params["limit"] = 1
				limit = 500

				try:
					print ("Getting data 1....")
					response = requests.get(self.fetch_seed_api,params)
					response = response.json()
					total_seed_count = int(response["total_count"])
				except Exception as e:
					time.sleep(1)
					response = requests.get(self.fetch_seed_api,params)
					response = response.json()
					total_seed_count = int(response["total_count"])
					# print e
					# raise e

				start = 0
				print ("INIT: ",start,limit,total_seed_count)
				while start < total_seed_count:
					print (start,limit,total_seed_count)
					params["start"] = start
					params["limit"] = limit
					try:
						print ("Getting data 2....",start,limit)
						response = requests.get(self.fetch_seed_api,params)
						print ("Got data.. ")
						response = response.json()
					except Exception as e:
						time.sleep(5)
						print ("Getting data 2....",start,limit)
						response = requests.get(self.fetch_seed_api,params)
						print ("Got data.. ")
						response = response.json()
						# print "Error in getting data: ",e
						# raise e
					start = start + limit
					# pass
					print (len(response["data"]))
					seed_data = response["data"]
					toValidate_list = []
					for entry in seed_data:
						# print entry
						if len(toValidate_list) <= limit:
							entry["clustering_tech"] = account_info_dict["tech_to_run"].split(",")
							entry["account_ID"] = account_id
							entry["source"] = entry["seed_source"]

							# print " FETCH DATA COMP LIST: ",entry['comp_list']

							toValidate_list += [entry]
						else:
							# pass
							try:
								validation_output = self.validation_module.Validation(seed_list=toValidate_list)
								upload_status = self.updateUrlhMonitorTable(validation_output=validation_output)
								toValidate_list = []
							except Exception as e:
								raise e
					else:
						# pass
						try:
							validation_output = self.validation_module.Validation(seed_list=toValidate_list)
							upload_status = self.updateUrlhMonitorTable(validation_output=validation_output)
							toValidate_list = []
						except Exception as e:
							print (e)
							raise e
				self.pushAccountToClustering(account_ID=account_id)
			except Exception as e:
				print ("Final error: ",e)
				raise e




	@cherrypy.expose
	@cherrypy.tools.json_in()
	@cherrypy.tools.json_out()
	def getDataFromTools(self,**kwargs):
	# def getDataFromTools(self,data=None,config=None):

		data = cherrypy.request.json
		data = json.loads(data)
		seed_data = data["data"]
		start = data["start"]
		finish = data["finish"]
		account_ID = data["account_ID"]
		seed_data = json.loads(seed_data)

		toValidate_list = []
		for entry in seed_data:
			entry = json.loads(entry)
			if len(toValidate_list) < 50:
				toValidate_list += [entry]
			else:
				try:
					validation_output = self.validation_module.Validation(seed_list=toValidate_list)
					upload_status = self.updateUrlhMonitorTable(validation_output=validation_output)
					toValidate_list = []
				except Exception as e:
					raise e
		else:
			try:
				validation_output = self.validation_module.Validation(seed_list=toValidate_list)
				upload_status = self.updateUrlhMonitorTable(validation_output=validation_output)
				toValidate_list = []
			except Exception as e:
				raise e
		print (start,finish)
		if start == finish:
			self.pushAccountToClustering(account_ID=account_ID)


	def encodeEntry(self,entry):

		temp_entry = {}
		for key in entry:
			try:
				temp_entry[key] = re.sub(r'[^\x00-\x7f]',r' ',entry[key])
			except Exception as e:
				print(e)
				temp_entry[key] = entry[key]

		return temp_entry

	def updateUrlhMonitorTable(self,validation_output):

		print("IN updateUrlhTrackingTable...")
		data_to_update = []
		for entry in validation_output:

			entry = self.encodeEntry(entry)

			print("Updted ... ", entry)
			validation_entry = entry["validation_output"]
			tech_to_run_list = entry["clustering_tech"]

			for tech in tech_to_run_list:
				temp_dict = {}
				temp_dict["urlh"] = entry["urlh"]
				temp_dict["account_ID"] = entry["account_ID"]
				temp_dict["clustering_status"] = "FALSE"
				# temp_dict["comp_list"] = ["Amazon-US"]
				temp_dict["seed_json"] = json.dumps(entry)
				if tech in validation_entry:
					tech_validation_entry = validation_entry[tech]
					temp_dict["tech_to_run"] = tech
					if len(tech_validation_entry) > 0:
						temp_dict["validation_flag"] = validation_entry[tech]["status"]
						temp_dict["validation_json"] = json.dumps(validation_entry[tech])
						data_to_update += [temp_dict]
					else:
						temp_dict["validation_flag"] = "FAIL"
						temp_dict["validation_json"] = None
						data_to_update += [temp_dict]

		try:
			self.sqloperations.updateUrlhTrackingTable(data_to_update=data_to_update)
			return 200
		except Exception as e:
			print(e)
			raise e


	def pushAccountToClustering(self,account_ID):
		account_info_dict = self.sqloperations.getAccountInfo(account_ID=account_ID)
		print (account_info_dict)
		account_ID = account_info_dict["account_ID"]
		tech_to_run = account_info_dict["tech_to_run"]
		account_name = account_info_dict["account_name"]
		seed_source = account_info_dict["seed_source"]

		account_validation_status = self.sqloperations.checkValidationStatus(account_ID=account_ID,tech_to_run=tech_to_run)
		print (account_validation_status)

		body = ""
		subject = "Account Health Report: " + account_ID

		temp_list = []
		for tech in account_validation_status:
			clustering_status = account_validation_status[tech]["clustering_status"]
			if clustering_status == "TRUE":
				kafkapush_status = self.sqloperations.pushToKafka(account_ID=account_ID,clutering_tech=tech,
																account_name=account_name,seed_source=seed_source)

			if clustering_status == "FALSE":
				temp_list += [tech]

		print ("sending Mail")
		try:

			body = "Account ID: " + account_ID + "\n"
			body += "Account name: " + account_name + "\n"
			body += "Seed source: " + seed_source + "\n"

			for key in account_validation_status:

				if key in temp_list:
					body += "******* CLUSTERING IS NOT STARTED FOR THIS *****\n"
				else:
					body += "******* CLUSTERING IS STARTED FOR THIS *****\n"

				body += "Clustering Tech: " + key + "\n"
				body += "Missing keys (needed): " + json.dumps(account_validation_status[key]["missing_needed"]) + "\n"
				body += "Missing keys (optional): " + json.dumps(account_validation_status[key]["missing_optional"]) + "\n"
				pass_count = 0
				fail_count = 0
				if "PASS" in account_validation_status[key]:
					pass_count = account_validation_status[key]["PASS"]
				if "FAIL" in account_validation_status[key]:
					fail_count = account_validation_status[key]["FAIL"]
				body += "Passed seed count: " + str(pass_count) + "\n"
				body += "Failed seed count: " + str(fail_count) + "\n"
				body += "\n"

			self.sendingMail(body=body, subject= subject)
		except Exception as e:
			raise e

		# return account_ID,account_validation_status


if __name__ == '__main__':

	# config_filepath = sys.argv[1]
	config_filepath = "./config/distributor_config.conf"
	config = {
		'global' : {
			'server.socket_host' : '0.0.0.0',
			'server.socket_port' : 9996,
			'server.thread_pool' : 8
		}
	}
	cherrypy.quickstart(Distributor(config_filepath=config_filepath), '/', config)
