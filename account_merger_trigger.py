from collections import defaultdict
import pymysql
pymysql.install_as_MySQLdb()
import MySQLdb
# import mysql.connector
import mysqlOperations
import time
import sys
import os


sys.path.append(os.path.abspath("/home/semantics/data/product_matching_merger/src/main/"))

from merger_main import Merger

merger_obj = Merger()

# mydb = mysql.connector.connect(
#   host="localhost",
#   user="root",
#   passwd="dataweave",
#   database="gtg_mysql"
# )

mydb = MySQLdb.connect(
  host="localhost",
  user="root",
  passwd="dataweave",
  database="gtg_mysql"
)
# sql = "select account from account_active"
mycursor = mydb.cursor()

#sql = "INSERT INTO URLH_TRACKING_TABLE (urlh, account_ID, tech_to_run, validation_flag,validation_json, seed_json,clustering_status) VALUES (%s, %s, %s, %s, %s, %s)"
# val = ("1126","clothing","CBIR","ACTIVE","FALSE")
# sql = "INSERT INTO ACCOUNT_TRACKING_TABLE (account_ID, category, tech_to_run, account_status, run_clustering_flag) VALUES (%s, %s, %s, %s, %s)"
acc_sql = "SELECT ACCOUNT_ID, TECH_TO_RUN FROM ACCOUNT_TRACKING_TABLE WHERE ACCOUNT_STATUS='ACTIVE' AND RUN_CLUSTERING_FLAG='TRUE' AND MERGED_STATUS='FALSE'"
mycursor.execute(acc_sql)
myresult = mycursor.fetchall()

# print(myresult)

mysqlobj = mysqlOperations.SqlOperations()

if myresult is not None:
	for i in myresult:
		acc_id = str(i[0])
		print(acc_id)
		techs_len = len(i[1].split(','))
		urlh_query = "SELECT URLH, TECH_TO_RUN, CLUSTERING_STATUS FROM URLH_TRACKING_TABLE WHERE ACCOUNT_ID = %s AND MERGED_STATUS = %s AND (CLUSTERING_STATUS = 'TRUE' OR CLUSTERING_STATUS='NOT CONSIDERED') "
		val = (acc_id, 'FALSE')
		mycursor.execute(urlh_query,val)
		urlhs_output = mycursor.fetchall()
		result_dict = {}
		for i in urlhs_output:
			if i[0] in result_dict.keys():
				# print "CLUSTERING_STATUS : ",i[2]
				if i[2] =='NOT CONSIDERED':
					result_dict[i[0]]['TOTAL_TECH'].append(i[1]) 
				else:
					result_dict[i[0]]['TECH_TO_RUN_CONSIDER'].append(i[1]) 
					result_dict[i[0]]['TOTAL_TECH'].append(i[1])

			else:
				result_dict[i[0]] = {}
				result_dict[i[0]]['TECH_TO_RUN_CONSIDER'] = []
				result_dict[i[0]]['TOTAL_TECH'] = []
				# print "CLUSTERING_STATUS : ",i[2]
				if i[2] == 'NOT CONSIDERED':
					result_dict[i[0]]['TOTAL_TECH'].append(i[1]) 
				else:
					result_dict[i[0]]['TECH_TO_RUN_CONSIDER'].append(i[1]) 
					result_dict[i[0]]['TOTAL_TECH'].append(i[1])
		print(result_dict)
		# print(len(all_urlhs))
		if len(result_dict)==0:
			print("hello")
		else:
			
			list_urlhs = []

			for urlh in result_dict:
				dic ={}
				dic["primary_keys"] = {}
				dic["keys_to_update"] = {}
				dic["primary_keys"]['urlh'] = urlh
				dic["account_ID"] = acc_id
				if result_dict[urlh]["TOTAL_TECH"] == result_dict[urlh]["TECH_TO_RUN_CONSIDER"]:
					dic["keys_to_update"]["MERGED_STATUS"] = 'IN PROGRESS'
					list_urlhs.append(dic)
					merger_obj.connect(account_ID=acc_id,seed=urlh)

			mysqlobj.updateEntry(tablename="URLH_TRACKING_TABLE", data_to_update=list_urlhs)

mydb.commit()
mydb.close()


# mydb = mysql.connector.connect(
#   host="localhost",
#   user="root",
#   passwd="dataweave",
#   database="gtg_mysql"
# )

mydb = MySQLdb.connect(
  host="localhost",
  user="root",
  passwd="dataweave",
  database="gtg_mysql"
)

mycursor = mydb.cursor()

acc_sql = "SELECT ACCOUNT_ID FROM ACCOUNT_TRACKING_TABLE WHERE ACCOUNT_STATUS='ACTIVE' AND RUN_CLUSTERING_FLAG='TRUE' AND MERGED_STATUS != 'TRUE'"
mycursor.execute(acc_sql)
myresult = mycursor.fetchall()

# print(myresult)


if myresult is not None:
	for i in myresult:
		acc_id = str(i[0])
		# mysqlobj1 = mysqlOperations.SqlOperations()
		sql = "select merged_status,count(urlh) from URLH_TRACKING_TABLE where account_ID = %s group by merged_status"
		account_ID = acc_id
		merged_status_val_list = []
		try:
			mycursor.execute(sql,[account_ID])
			myresult = mycursor.fetchall()
			for row in myresult:
				print(row)
				merged_status_val_list += [row[0].strip()]


			if len(merged_status_val_list) == 1 and merged_status_val_list[0] == "TRUE":
				print("FINISHED",acc_id)
				data_to_update = [{"primary_keys": {"account_ID":account_ID}, "keys_to_update": {"merged_status":"TRUE","upload_status":"TRUE"}}]
				mysqlobj.updateEntry(tablename="ACCOUNT_TRACKING_TABLE", data_to_update=data_to_update)
			else:
				print("ACCOUNT PROCRESSING......",acc_id)
		except Exception as e:
			print(e)

# sql = "select merged_status,count(urlh) from URLH_TRACKING_TABLE where account_ID = %s group by merged_status"


# try:
# 	mycursor.execute(sql)
# 	myresult = mycursor.fetchall()

# 	for row in myresult:

# except Exception as e:
# 	print e
mydb.commit()
mydb.close()
# acc_id = str(myresult[0][0] )

# # print(acc_id)
# # sql_parameterized_query = "select count(*) from URLH_TRACKING_TABLE where account_ID = (%s) and tech_to_run=(%s) and clustering_status='FALSE'"
# urlh_query = "SELECT URLH FROM URLH_TRACKING_TABLE WHERE ACCOUNT_ID = %s AND MERGED_STATUS = %S AND RUN_CLUSTERING_FLAG = 'TRUE' GROUP BY URLH HAVING COUNT(URLH) = %d "
# for i in z:
# 	inp = (acc_id,i)
# 	print(inp)
# 	mycursor.execute(sql_parameterized_query,inp)
# 	for x in mycursor:
# 		sum+=x[0]
# 		print(x[0])# print(type(z)

# if sum>0:
# 	mycursor.execute("update ACCOUNT_TRACKING_TABLE set MERGED_STATUS='IN PROCESS' where account_ID=(%s)",(acc_id,))
# 	mydb.commit()
# 	print(sum)# for i in z:
# # 	urlh_query



# mydb.close()
time.sleep(50)
