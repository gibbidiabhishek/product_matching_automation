# -*- coding: utf-8 -*-
# @Author: abhishek
# @Date:   2018-12-28 12:52:01
# @Last Modified by:   abhishek
# @Last Modified time: 2019-05-09 19:24:25

import pymysql
pymysql.install_as_MySQLdb()
import MySQLdb
from kafkalib import kafka_producer_product_matching
import json

class SqlOperations(object):
	"""docstring for SqlOperations"""
	def __init__(self):
		pass

	def connectMySql(self):
		# if self.db:
			# self.db.close()
		self.db = MySQLdb.connect("localhost","root","dataweave","gtg_mysql")
		self.cursor = self.db.cursor()		
	
	def test(self):
		# sql = "select count(*) from CLUSTERING_MATCH_DESCRIPTION_TABLE_DETAILED"
		sql = "desc CLUSTERING_MATCH_DESCRIPTION_TABLE_DETAILED"
		self.cursor.execute(sql)
		results = self.cursor.fetchall()
		print(results[0])

	def updateUrlhTrackingTable(self,data_to_update):
		self.connectMySql()
		sql = "insert into URLH_TRACKING_TABLE (urlh, account_ID, tech_to_run, validation_flag, validation_json, clustering_status, seed_json) values (%s ,%s, %s, %s, %s, %s, %s)"

		to_push_list = []
		for entry in data_to_update:
			
			urlh = entry["urlh"]
			account_ID = entry["account_ID"]
			tech_to_run = entry["tech_to_run"]
			validation_flag = entry["validation_flag"]
			validation_json = entry["validation_json"]
			# validation_json = "{}"
			if validation_flag != "FAIL":
				clustering_status = entry["clustering_status"]
			else:
				clustering_status = "NOT CONSIDERED"	
			seed_json = entry["seed_json"]			
			# seed_json = "{}"
			try:
				val_entry = (urlh,account_ID,tech_to_run,validation_flag,validation_json,clustering_status,seed_json)
				print("Inserting Values: ", val_entry[0:4])
				status = self.cursor.execute(sql,val_entry)
				print("SQL: ",status)
				
			except Exception as e:

				if e[0] == "1062": #duplicate entry flag-- updating the alredy existing entry
					sql1 = "update URLH_TRACKING_TABLE set validation_flag = %s, validation_json = %s, clustering_status = %s where urlh = %s and account_ID = %s and  tech_to_run = %s"
					print("SQL Error: ", e[0])
					try:
						val_entry = (validation_flag,validation_json,clustering_status,urlh,account_ID,tech_to_run)
						print("Inserting Values except: ", val_entry)
						status = self.cursor.execute(sql1,val_entry)
						print("SQL: ",status)
					except Exception as e:
						print(e)
						raise e
		try:
			self.db.commit()
			# self.db.close()
			return 200			
		except Exception as e:
			# self.db.close()
			raise e		
		
	def getAccountInfo(self,account_ID):
		self.connectMySql()
		sql = "select * from ACCOUNT_TRACKING_TABLE where account_ID = " + '"' + account_ID + '"'
		try:
			self.cursor.execute(sql)
			results = self.cursor.fetchall()[0]
			account_info_dict = {}
			account_info_dict["account_ID"] = results[0]
			account_info_dict["category"] = results[1]
			account_info_dict["tech_to_run"] = results[2]
			account_info_dict["account_status"] = results[3]
			account_info_dict["run_clustering_flag"] = results[4]
			account_info_dict["merged_status"] = results[5]
			account_info_dict["upload_status"] = results[6]
			account_info_dict["account_name"] = results[8]
			account_info_dict["seed_source"] = results[9]
			return account_info_dict
		except Exception as e:
			raise e

	def insertToAccountTracking(self,account_ID,account_name,seed_source,tech_to_run):
		account_ID = account_ID
		category = "Dummy"
		tech_to_run= tech_to_run
		account_status = "ACTIVE"
		run_clustering_flag = "TRUE"
		merged_status = "FALSE"
		upload_status = "FALSE"
		account_name = 	account_name	
		seed_source = seed_source

		self.connectMySql()

		try:
			sql = "insert into ACCOUNT_TRACKING_TABLE (account_ID, category, tech_to_run, account_status, run_clustering_flag, merged_status, upload_status, account_name, seed_source) values (%s,%s,%s,%s,%s,%s,%s,%s,%s)"
			val_entry = (account_ID,category,tech_to_run,account_status,run_clustering_flag,merged_status,upload_status,account_name,seed_source)
			status = self.cursor.execute(sql,val_entry)
			self.db.commit()
		except Exception as e:
			print (e)
			raise e

	def checkValidationStatus(self,account_ID,tech_to_run):
		self.connectMySql()
		config_techs = tech_to_run.split(",")
		
		sql = "select tech_to_run, validation_flag,count(urlh) from URLH_TRACKING_TABLE where account_ID = %s group by validation_flag,tech_to_run"
		
		try:
			self.cursor.execute(sql,[account_ID])		
			results = self.cursor.fetchall()
			status_dict = {}
			for r in results:
				tech,status,count = r
				if tech not in status_dict and tech in config_techs:
					status_dict[tech] = {}
				
				if tech in status_dict:	
					status_dict[tech][status] = int(count)
		except Exception as e:
			raise e		

		for tech in status_dict:
			pass_count = 0
			fail_count = 0
			if "PASS" in status_dict[tech]:
				pass_count = status_dict[tech]["PASS"]
			if "FAIL" in status_dict[tech]:
				fail_count = status_dict[tech]["FAIL"]
			
			percentage = (float(pass_count)/float(pass_count+fail_count) ) * 100.0
			status_dict[tech]["percentage"] = percentage

		for tech in status_dict:
			if status_dict[tech]["percentage"] > float(60):
				status_dict[tech]["clustering_status"] = "TRUE"
			else:
				status_dict[tech]["clustering_status"] = "FALSE"

			sql = "select validation_json from URLH_TRACKING_TABLE where account_ID = %s and tech_to_run = %s and validation_flag = %s"
			try:
				self.cursor.execute(sql,[account_ID,tech,"FAIL"])
				results = self.cursor.fetchall()
				missing_needed_list = set()
				missing_optional_list = set()
				for r in results:
					validation_json = json.loads(r[0])
					missing_needed = validation_json["missing_needed"]
					missing_optional = validation_json["missing_optional"]
					missing_needed_list = missing_needed_list.union(missing_needed)
					missing_optional_list = missing_optional_list.union(missing_optional)

				status_dict[tech]["missing_needed"] = list(missing_needed_list)
				status_dict[tech]["missing_optional"] = list(missing_optional_list)

			except Exception as e:
				raise e				
		
		# print status_dict	
		return status_dict	

	def pushToKafka(self,account_ID,clutering_tech,account_name,seed_source):
		self.connectMySql()
		print("PUSHING TO KAFKA....")
		# kafkaObj = kafka_producer.kafka() 

		sql = "select urlh,seed_json from URLH_TRACKING_TABLE where account_ID = %s and tech_to_run = %s and validation_flag = %s and clustering_status = %s"

		try:
			# print(account_ID,clutering_tech)

			# print(sql,account_ID,clutering_tech)

			try:
				self.cursor.execute(sql,[account_ID,clutering_tech,"Pass","FALSE"])
				# print("DONE 1")
				results = self.cursor.fetchall()
				# print("DONE")
			except Exception as e:
				raise e

			# print (len(results))

			push_to_kafka = []
			data_to_update = []
			for r in results:
				# print r
				urlh = r[0]
				# print (urlh)
				# seed_json = r[1].decode('utf-8','ignore').encode("utf-8")
				seed_json = r[1]

				try:
					seed_json = json.loads(seed_json)
					push_to_kafka += [seed_json]	
				except Exception as e:
					try:
						seed_json = seed_json.encode('utf-8')
						seed_json = json.loads(seed_json)
						push_to_kafka += [seed_json]						
					except Exception as e:
						print (seed_json)
						seed_json = seed_json.decode('utf-8').encode('utf-8')
						seed_json = json.loads(seed_json)
						push_to_kafka += [seed_json]
						
				temp_dict = {}
				temp_dict["primary_keys"] = {"account_ID": account_ID,"tech_to_run":clutering_tech,"urlh":urlh}
				temp_dict["keys_to_update"] = {"clustering_status": "IN PROGRESS"}
				
				data_to_update += [temp_dict]
				
				kafka_index_key = clutering_tech.lower() + "_index_id"
				kafka_index_id  = account_ID + "_" + seed_source +"_"+ clutering_tech
				seed_json[kafka_index_id] = kafka_index_key

				

				if len(push_to_kafka) % 100 == 0:
					if clutering_tech == "TEXT":
						print ("PUSHING to TEXT")
						# pass

						kafka_producer_product_matching.pustToKafka("sm_input_text_clustering",push_to_kafka)

					if clutering_tech == "PPC":
						print ("PUSHING to PPC")
						# pass
						kafka_producer_product_matching.pustToKafka("sm_input_ppc_clustering",push_to_kafka)

					if clutering_tech == "CBIR":
						print ("PUSHING TO CBIR")
						kafka_producer_product_matching.pustToKafka("sm_input_cbir_clustering",push_to_kafka)
					push_to_kafka = [] 		
			else:
				if len(push_to_kafka) > 0:

					if clutering_tech == "TEXT":
						print ("PUSHING to TEXT")
						# pass
						kafka_producer_product_matching.pustToKafka("sm_input_text_clustering",push_to_kafka)

					if clutering_tech == "PPC":
						print ("PUSHING to PPC")
						# pass
						kafka_producer_product_matching.pustToKafka("sm_input_ppc_clustering",push_to_kafka)

					if clutering_tech == "CBIR":
						print ("PUSHING to CBIR")
						# pass
						kafka_producer_product_matching.pustToKafka("sm_input_cbir_clustering",push_to_kafka)		

			self.updateEntry(tablename="URLH_TRACKING_TABLE", data_to_update=data_to_update)

		except Exception as e:
			print (e)
			raise e

		return 200	

	def updateEntry(self,tablename,data_to_update):
	
		try:
			self.connectMySql()	
		except Exception as e:
			raise e
		
		for entry in data_to_update:		
			primary_keys_entry = entry["primary_keys"]
			keystoupdate_entry = entry["keys_to_update"]

			primary_keys_str = ""
			update_str = ""
			for key in primary_keys_entry:
				# print(key)
				# print(primary_keys_str)
				primary_keys_str += key + " = %s and "

			primary_keys_values = primary_keys_entry.values()

			
			for key in keystoupdate_entry:
				update_str += key + " = %s,"

			update_str_values = keystoupdate_entry.values()
			

			sql = "update " + tablename +  " set "+ update_str[:-1] + " where " + primary_keys_str[:-4] 
			args_values = list(update_str_values) + list(primary_keys_values)

			try:
				self.cursor.execute(sql, args_values)
			except Exception as e:
				print("Error: ",e,entry)
				raise e
		self.db.commit()		


if __name__ == '__main__':
	
	mysqlobj = SqlOperations()

	# mysqlobj.getAccountInfo(account_ID="1111")
	# mysqlobj.checkValidationStatus(account_ID="1111", tech_to_run="TEXT,PPC")
	data_to_update = [{"primary_keys": {"account_ID":1111,"urlh": "131245","tech_to_run": "CBIR"}, "keystoupdate": {"clustering_status":"TRUE"}}]
	mysqlobj.updateEntry(tablename="test", data_to_update=data_to_update)