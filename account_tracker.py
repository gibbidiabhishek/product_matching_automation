# -*- coding: utf-8 -*-
# @Author: abhishek
# @Date:   2019-04-12 15:59:18
# @Last Modified by:   abhishek
# @Last Modified time: 2019-06-21 14:15:28

import sys
import json
import requests
import cherrypy
import time
import os
import argparse
import urllib
from collections import OrderedDict
from configobj import ConfigObj
from cherrypy.process.plugins import BackgroundTask
import pymysql
pymysql.install_as_MySQLdb()
import MySQLdb

from mysqlOperations import SqlOperations


class AccountTracker(object):
	"""docstring for AccountTracker"""

	def __init__(self):
		# super(AccountTracker, self).__init__()
		# self.arg = arg
		self.sqloperations = SqlOperations()
		# pass

	def connectMySql(self):
		# if self.db:
			# self.db.close()
		self.db = MySQLdb.connect("localhost","root","dataweave","gtg_mysql")
		self.cursor = self.db.cursor()

	def getAccountInfo(self,account_id):

		account_info_dict = self.sqloperations.getAccountInfo(account_ID=account_id)
		return account_info_dict

	def getValidationStatus(self,account_id,tech_to_run):

		account_validation_status = self.sqloperations.checkValidationStatus(account_ID=account_id,tech_to_run=tech_to_run)
		return account_validation_status

	def getAccountProgress(self,account_id):

		self.connectMySql()
		sql = "select tech_to_run,clustering_status,merged_status,count(urlh) from URLH_TRACKING_TABLE where account_ID = %s group by clustering_status,tech_to_run,merged_status"

		status_list = []
		finished_count = 0
		unfinished_count = 0

		progress_dict = {}


		try:
			self.cursor.execute(sql,[account_id])
			results = self.cursor.fetchall()

			for row in results:
				temp_dict = {}
				temp_dict["tech_to_run"] = row[0]
				temp_dict["clustering_status"] = row[1]
				temp_dict["merged_status"] = row[2]
				temp_dict["urlh_count"] = int(row[3])
				if row[1] == "TRUE":
					finished_count += int(row[3])
				else:
					unfinished_count += int(row[3])
				status_list.append(temp_dict)
		except Exception as e:
			raise e

		progress_dict["Finished Count"] = finished_count
		progress_dict["Running Count"] = unfinished_count

		return status_list, progress_dict

	def getCompetitorLevelAnalysis(self,account_id,account_info_dict):

		# print(account_info_dict)
		self.connectMySql()
		sql = "select docs_detail,competitor_list from CLUSTERING_MATCH_DESCRIPTION_TABLE_DETAILED where account_ID = %s"
		competitorLevel_clusterdict = {}
		try:
			self.cursor.execute(sql,[account_id])
			results = self.cursor.fetchall()

			for row in results:
				# print(row)
				docs_detail = json.loads(row[0])
				if row[1] != "None":
					competitor_list = row[1].split(",")
				else:
					competitor_list = []
				# print(competitor_list)
				for comp in competitor_list:
					if comp not in competitorLevel_clusterdict:
						competitorLevel_clusterdict[comp] = {}
						competitorLevel_clusterdict[comp]["singleton"] = 0
						competitorLevel_clusterdict[comp]["bundle"] = 0

				clusted_comp_list = docs_detail.keys()
				missing_comp_list = list(set(competitor_list) - set(clusted_comp_list))
				# print(clusted_comp_list)
				for clu_comp in clusted_comp_list:
					competitorLevel_clusterdict[clu_comp]["bundle"] += 1
				for singleton_comp in missing_comp_list:
					competitorLevel_clusterdict[singleton_comp]["singleton"] += 1
			print(competitorLevel_clusterdict)
			return competitorLevel_clusterdict
		except Exception as e:
			# raise e
			return {}

	def getAfterQABundleAnalysis(self,account_ID):
		self.connectMySql()
		sql = "select * from VERIFIED_MATCH_DESCRIPTION_TABLE where account_ID like '" + str(account_ID) + "%'"
		print(sql)
		after_qa_match_count = {}
		try:
			self.cursor.execute(sql)
			results = self.cursor.fetchall()

			for row in results:
				matched_docs = row[2]
				matched_docs_list = matched_docs.split(",")
				if '' in matched_docs_list:
					matched_docs_list.remove('')
				# if len(matched_docs_list) == 3:
					# print(row)
				if len(matched_docs_list) not in after_qa_match_count:
					after_qa_match_count[len(matched_docs_list)] = 1
				else:
					after_qa_match_count[len(matched_docs_list)] += 1
				# print(row)
			print(after_qa_match_count)
			return after_qa_match_count
		except Exception as e:
			return after_qa_match_count
			# raise e



	@cherrypy.expose
	def showAccountStatus(self,account_id):

		# account_id = cherrypy.request.params['account_id']
		account_info_dict = self.getAccountInfo(account_id)
		tech_to_run = account_info_dict["tech_to_run"]
		account_validation_status = self.getValidationStatus(account_id, tech_to_run)
		status_list,progress_dict = self.getAccountProgress(account_id)
		competitorLevel_clusterdict = self.getCompetitorLevelAnalysis(account_id,account_info_dict)
		after_qa_match_count = self.getAfterQABundleAnalysis(account_id)
		display_dict = OrderedDict()

		display_dict["account_info"] = account_info_dict
		display_dict["account_validation_status"] = account_validation_status
		display_dict["account_status"] = status_list
		display_dict["account_progress_status"] = progress_dict
		display_dict["competitor_wise_count_after_cluster"] = competitorLevel_clusterdict
		display_dict["after_qa_match_count"] = after_qa_match_count

		print (display_dict)
		return json.dumps(display_dict)

if __name__ == '__main__':

	# config = {
	# 	'global' : {
	# 		'server.socket_host' : '0.0.0.0',
	# 		'server.socket_port' : 9999,
	# 		'server.thread_pool' : 8
	# 	}
	# }
	# cherrypy.quickstart(AccountTracker(), '/', config)

	AccountTracker_obj = AccountTracker()
	account_id = "9118"
	account_info_dict = AccountTracker_obj.getAccountInfo(account_id)
	# AccountTracker_obj.getCompetitorLevelAnalysis(account_id,account_info_dict)
	AccountTracker_obj.getAfterQABundleAnalysis(account_id)

	# print account_info_dict
	# tech_to_run = account_info_dict["tech_to_run"]
	# account_validation_status = AccountTracker_obj.getValidationStatus(account_id, tech_to_run)

	# AccountTracker_obj.getAccountProgress(account_id)

